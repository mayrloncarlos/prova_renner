# Automação de Testes de e-commerce

Projeto contendo a automação dos testes da aplicação de e-commerce de roupas 

## Pré-requisitos

Java 8
Plugin do TestNG

Navegadores Firefox e Chrome

## Principais arquivos do projeto para rodar a automação

- config.properties - arquivo com as configurações gerais da automação. Exemplo: tipo do navegador, url para acessar a aplicação, tipo de execução, etc.

## Parâmetros chaves a serem modificados em cada um dos arquivos

- config.properties
    - browser.name: Nome do navegador;
    - retry.limit: Quantidade de vezes para reexecutar testes com falha;
    - site.address: Endereço da aplicação;
    - tipo.execucao: Local ou em grid (Servidor);
    - headless: True para abrir navegador e False para execução do teste sem abrir navegador;
    - dimensao.personalizada: True para ativar a resolução personalizada e False para usar resolução da tela principal;
    - largura: largura da resolução;
    - altura: altura da resolução.

## Rodando os testes

- Rodar todos os testes de uma classe de teste
    - Entrar na classe de teste -> Clicar com o botão direito -> Run as TestNG Test

- Rodar apenas um caso de teste
    - Entrar na classe de teste -> Selecionar método de teste -> Clicar com o botão direito -> Run as TestNG Test
