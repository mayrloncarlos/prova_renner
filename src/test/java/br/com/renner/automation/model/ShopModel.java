package br.com.renner.automation.model;

public class ShopModel {

    String printedChiffonDress;
    String fadedShortSleeve;
    String blouse;

    public static ShopModel gerarInformacoesShopModel(){
        ShopModel shopModel = new ShopModel();
        shopModel.setPrintedChiffonDress("Printed Chiffon Dress");
        shopModel.setFadedShortSleeve("Faded Short Sleeve T-shirts");
        shopModel.setBlouse("Blouse");
        return shopModel;
    }

    public String getPrintedChiffonDress() {
        return printedChiffonDress;
    }

    public void setPrintedChiffonDress(String printedChiffonDress) {
        this.printedChiffonDress = printedChiffonDress;
    }

    public String getFadedShortSleeve() {
        return fadedShortSleeve;
    }

    public void setFadedShortSleeve(String fadedShortSleeve) {
        this.fadedShortSleeve = fadedShortSleeve;
    }

    public String getBlouse() {
        return blouse;
    }

    public void setBlouse(String blouse) {
        this.blouse = blouse;
    }
}
