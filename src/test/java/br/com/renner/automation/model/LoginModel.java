package br.com.renner.automation.model;

import br.com.renner.automation.util.GenerateRandom;

public class LoginModel {

    String emailPreCadastro;
    String firstName;
    String lastName;
    String password;
    String firsNameAddress;
    String lastNameAddress;
    String address;
    String city;
    String state;
    String zipCode;
    String country;
    String phone;

    public static LoginModel gerarInformacoesLoginModel(){
        LoginModel loginModel = new LoginModel();
        loginModel.setEmailPreCadastro("mayrlonTeste"+ GenerateRandom.gerarNumeroRandomico()+"@teste.com");
        loginModel.setFirstName("Usuário");
        loginModel.setLastName("Teste");
        loginModel.setPassword("12345");
        loginModel.setFirsNameAddress("Endereço");
        loginModel.setLastNameAddress("Teste");
        loginModel.setAddress("Rua das Flores, 350");
        loginModel.setCity("João Pessoa");
        loginModel.setState("1");
        loginModel.setZipCode("54123");
        loginModel.setCountry("21");
        loginModel.setPhone("83999999999");
        return loginModel;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirsNameAddress() {
        return firsNameAddress;
    }

    public void setFirsNameAddress(String firsNameAddress) {
        this.firsNameAddress = firsNameAddress;
    }

    public String getLastNameAddress() {
        return lastNameAddress;
    }

    public void setLastNameAddress(String lastNameAddress) {
        this.lastNameAddress = lastNameAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmailPreCadastro() {
        return emailPreCadastro;
    }

    public void setEmailPreCadastro(String emailPreCadastro) {
        this.emailPreCadastro = emailPreCadastro;
    }
}
