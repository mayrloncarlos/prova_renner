package br.com.renner.automation.common;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Hashtable;
import java.util.Map;

public class Chrome {
	
	public static ChromeOptions configChromeOptions() {
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("disable-infobars");

		String downloadFilepath = System.getProperty("java.io.tmpdir");
		Map<String, Object> preferences = new Hashtable<String, Object>();
		preferences.put("profile.default_content_settings.popups", 0);
		preferences.put("download.prompt_for_download", "false");
		preferences.put("download.default_directory", downloadFilepath);
		preferences.put("browser.setDownloadBehavior", "allow");

		// disable flash and the PDF viewer
		preferences.put("plugins.plugins_disabled", new String[]{
		    "Adobe Flash Player", "Chrome PDF Viewer"});

		options.setExperimentalOption("prefs", preferences);

		//Set browser to headless
		if(Property.HEADLESS) {
			options.setHeadless(true);
		}

		return options;
	}

	public static ChromeDriver configChromeDriver() {
		System.setProperty("webdriver.chrome.driver", Property.CHROME_DRIVE_PATH);
		ChromeOptions options = configChromeOptions();
		return new ChromeDriver(options);
	}
}
