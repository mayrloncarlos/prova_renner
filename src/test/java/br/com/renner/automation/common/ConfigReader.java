package br.com.renner.automation.common;

import br.com.renner.automation.enums.BrowserType;
import br.com.renner.automation.enums.ExecutionType;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigReader {
	
	public static void loadProperties() {
		ConfigReader reader = new ConfigReader();
		reader.readConfigProperty();
	}

	private void readConfigProperty() {
		Properties p = new Properties();
		try {
			FileInputStream inputStream = new FileInputStream(Property.propFileConfig);
			p.load(inputStream);
			
			Property.FIREFOX_DRIVE_PATH         = new File("").getAbsolutePath() + "\\src\\test\\resources\\driver\\windows\\geckodriver.exe";
			Property.CHROME_DRIVE_PATH 			= new File("").getAbsolutePath() + "\\src\\test\\resources\\driver\\windows\\chromedriver.exe";

			Parametro.BROWSER				.set(BrowserType.valueOf(p.getProperty("browser.name")));
			Property.RETRY_LIMIT			= Integer.parseInt(p.getProperty("retry.limit"));
			Property.TIPO_EXECUCAO 			= ExecutionType.valueOf(p.getProperty("tipo.execucao"));

			Property.SITE_ADDRESS 				= p.getProperty("site.address");
			Property.SELENIUM_GRID_HUB          = p.getProperty("selenium.grid.hub");
			Property.HEADLESS					= Boolean.parseBoolean(p.getProperty("headless"));
			Property.DIMENSAO_PERSONALIZADA		= Boolean.parseBoolean(p.getProperty("dimensao.personalizada"));
			Property.LARGURA_TELA				= Integer.parseInt(p.getProperty("largura"));
			Property.ALTURA_TELA				= Integer.parseInt(p.getProperty("altura"));
		} catch (Exception e) {
		}
	}
}