package br.com.renner.automation.common;

import org.openqa.selenium.remote.RemoteWebDriver;

public class Selenium {
	
	private static ThreadLocal<RemoteWebDriver> remoteWebDriverThreadLocal = new ThreadLocal<>();

	public static RemoteWebDriver getDriver() {
		return remoteWebDriverThreadLocal.get();
	}

	static void setDriver(RemoteWebDriver driverThreadLocal) {
		remoteWebDriverThreadLocal.set(driverThreadLocal);
	}
	
	public static void resetDriver() {
		RemoteWebDriver driver = getDriver();
		if(driver != null) {
			driver.quit();
			driver = null;
		}
	}
	
}