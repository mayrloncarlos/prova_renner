package br.com.renner.automation.common;

import br.com.renner.automation.enums.BrowserType;
import br.com.renner.automation.enums.ExecutionType;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import static br.com.renner.automation.common.Selenium.getDriver;
import static br.com.renner.automation.common.Selenium.setDriver;

public class Browser {

    public static void startDriver(BrowserType browser, ExecutionType execution) {
        RemoteWebDriver driver = null;

        switch (browser){

            case chrome:{
                switch (execution) {
                    case local:{
                        driver = Chrome.configChromeDriver();
                        break;
                    }
                    case grid:
                        try {
                            driver = new RemoteWebDriver(new URL(Property.SELENIUM_GRID_HUB), Chrome.configChromeOptions());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        break;
                }
                setDriver(driver);
                break;

            }
            case firefox:{
                switch (execution) {
                    case local:{
                        driver = Firefox.configFirefoxDriver();
                        break;
                    }
                    case grid:
                        try {
                            driver = new RemoteWebDriver(new URL(Property.SELENIUM_GRID_HUB), Firefox.configFirefoxOptions());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        break;
                }
                setDriver(driver);
                break;
            }
        }
    }

    public static void goToUrl(String url) {
        getDriver().get(url);
    }

    public static void maximize() {
        getDriver().manage().window().maximize();
    }

    public static void dimensaoPersonalizada() {
        getDriver().manage().window().setSize(new Dimension(Property.LARGURA_TELA, Property.ALTURA_TELA));
    }
}
