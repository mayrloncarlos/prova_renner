package br.com.renner.automation.common;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class Firefox {

	public static FirefoxOptions configFirefoxOptions() {
		FirefoxOptions options = new FirefoxOptions();
		FirefoxProfile profile = new FirefoxProfile();

		//Set browser to headless
		if(Property.HEADLESS) {
			options.setHeadless(true);
		}

		//Set profile to accept untrusted certificates
		profile.setAcceptUntrustedCertificates(true);

		//Set profile to not assumet certificate issuer is untrusted
		profile.setAssumeUntrustedCertificateIssuer(false);

		//Disable Firebug
		profile.setPreference("extensions.firebug.showFirstRunPage", false);
		profile.setPreference("extensions.firebug.currentVersion", "999");
		
		//Set download location and file types
		options.addPreference("browser.download.folderList",2);
		options.addPreference("browser.download.manager.showWhenStarting",false);
		if(Property.TIPO_EXECUCAO.toString().equals("grid")) {
			options.addPreference("browser.download.dir", "/home/seluser/");
		}else {
			options.addPreference("browser.download.dir", System.getProperty("java.io.tmpdir"));
		}
		options.addPreference("browser.helperApps.neverAsk.saveToDisk","text/csv,application/pdf,application/csv,application/vnd.ms-excel,application/aspx,text/html,text/xhtml,application/xhtml+xml,application/xml");

		//Set to false so popup not displayed when download finished.
		options.addPreference("browser.download.manager.showAlertOnComplete",false);

		options.addPreference("browser.download.panel.shown",false);
		options.addPreference("browser.download.useToolkitUI",true);

		//Set this to true to disable the pdf opening
		options.addPreference("pdfjs.disabled", true);
		options.addPreference("pdfjs.enabledCache.state", false);

		//Desabilita botao de imprimir
		options.addPreference("print.always_print_silent", true);
		options.addPreference("print.show_print_progress", false);

		//Desabilita o proxy
		options.addPreference("network.proxy.type", 0);
		
		options.setProfile(profile);
		
		//Desabilita o cache
//		profile.setPreference("browser.cache.disk.enable", false);
//		profile.setPreference("browser.cache.memory.enable", false);
//		profile.setPreference("browser.cache.offline.enable", false);
		
		//Desabilita logs marionette
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
		
		return options;
	}

	public static FirefoxDriver configFirefoxDriver() {
		System.setProperty("webdriver.gecko.driver", Property.FIREFOX_DRIVE_PATH);
		FirefoxOptions options = configFirefoxOptions();
		return new FirefoxDriver(options);
	}
	
}