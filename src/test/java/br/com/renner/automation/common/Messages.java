package br.com.renner.automation.common;

public class Messages {

    public static final String MSG_USUARIO_LOGADO      = "Usuário Teste";
    public static final String MSG_ADD_TO_CART_SUCCESS = "Product successfully added to your shopping cart";
    public static final String MSG_COMPRA_EFETUDA      = "Your order on My Store is complete.";
}
