package br.com.renner.automation.common;


import br.com.renner.automation.enums.ExecutionType;

public abstract class Property {

	//arquivos
	public static final String propFileConfig = "src/test/resources/config/config.properties";

	//drivers
	public static String FIREFOX_DRIVE_PATH;
	public static String CHROME_DRIVE_PATH;

	//config.properties
	public static Integer RETRY_LIMIT;
	public static ExecutionType TIPO_EXECUCAO;
	public static String SELENIUM_GRID_HUB;
	public static Boolean HEADLESS;
	public static Boolean DIMENSAO_PERSONALIZADA;
	public static Integer ALTURA_TELA;
	public static Integer LARGURA_TELA;
	public static String SITE_ADDRESS;
}