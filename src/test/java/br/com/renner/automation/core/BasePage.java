package br.com.renner.automation.core;

import br.com.renner.automation.common.Selenium;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import static br.com.renner.automation.common.Selenium.getDriver;

public class BasePage {

    /**
     * Variável definida para ser utilizada nos métodos de espera.
     */
    private static final int LOAD_TIMEOUT = 30;

    public BasePage() {
        PageFactory.initElements(getDriver(), this);
    }

    //CLICKS
    /**
     * Método para clicar em um elemento.
     * @param element parâmetro element é o elemento a ser clicado
     */
    public void click(WebElement element) {
        try {
            aguardarElementoVisivel(element);
            isClickable(element);
            element.click();
        } catch (Exception e) {
            Assert.fail("Nao foi possivel encontrar o elemento para clicar: " + element + ". Pagina: " + getDriver().getTitle() + "\n " + e.getMessage());
        }
    }

    //ESPERAS
    /**
     * Método para aguardar um elemento da tela aparecer, o tempo de espera é definido na variável #LOAD_TIMEOUT.
     * @param element parâmetro element para esperar o elemento desejado aparecer
     */
    public void aguardarElementoVisivel(WebElement element) {
        try {
            new WebDriverWait(getDriver(), LOAD_TIMEOUT).until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e) {
            Assert.fail("Tempo excedido para aguardar elemento: " + element);
        }
    }

    // VISIBLE
    /**
     * Método para verificar se o elemento está clicável na tela.
     * @param element parâmetro element do tipo WebElement é o elemento a ser procurado
     */
    public void isClickable(WebElement element) {
        try {
            new WebDriverWait(Selenium.getDriver(), LOAD_TIMEOUT).until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {
            Assert.fail("Excedeu o tempo limite de espera para vereficar se o elemento e clicavel: "+ element);
        }
    }

    /**
     * Método para verificar se o elemento está na tela.
     * @param elemento parâmetro element é o elemento a ser procurado
     * @return retorna true se estiver na tela e false caso não esteja
     */
    public boolean isDisplayed(WebElement elemento) {
        try {
            return elemento.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    //TEXT
    /**
     * Método para preenchimento de um campo.
     * @param element parâmetro element é o campo a ser preenchido
     * @param keysToSend parâmetro keysToSend é o valor a ser preenchido no campo
     */
    public void preencherCampo(WebElement element, CharSequence... keysToSend) {
        try {
            aguardarElementoVisivel(element);
            element.clear();
            element.sendKeys(keysToSend);
        } catch (WebDriverException e) {
            Assert.fail("Nao foi possivel encontrar o elemento para preencher: " + element + ". Pagina: " + getDriver().getTitle() + "\n " + e.getMessage());
        }
    }

    /**
     * Método para obter o texto de um elemento.
     * @param element parâmetro element é o elemento da tela a ser capturado o texto
     * @return retorna o texto do elemento desejado
     */
    public String obterTexto(WebElement element) {
        aguardarElementoVisivel(element);
        if (!isDisplayed(element)) {
            Assert.fail("Erro ao buscar texto em tela. Elemento: [" + element + "]");
        }
        return element.getText();
    }

    /**
     * Método para selecionar um item de um dropdown.
     * @param element é o locator do select
     * @param value é o texto do option a ser selecionado
     */
    public void selecionarItemDropdown(WebElement element, String value){
        Select selectObject = new Select(element);
        selectObject.selectByValue(value);
    }

    /**
     * Método para limpar o campo de um elemento.
     * @param element parâmetro element é o campo da tela a ser limpo
     */
    public void limparCampo(WebElement element) {
        try {
            aguardarElementoVisivel(element);
            element.clear();
        } catch (WebDriverException e) {
            Assert.fail("Nao foi possivel limpar os dados do campo: " + element + ". Pagina: " + getDriver().getTitle() + "\n " + e.getMessage());
        }
    }
}
