package br.com.renner.automation.test;

import br.com.renner.automation.common.Messages;
import br.com.renner.automation.core.BaseTestCase;
import br.com.renner.automation.model.LoginModel;
import br.com.renner.automation.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTestCase {

    @Test
    public void criarNovoUsuario(){

        LoginModel loginModel = LoginModel.gerarInformacoesLoginModel();
        LoginPage loginPage = new LoginPage(loginModel);

        loginPage.criarNovoUsuario();

        Assert.assertEquals(loginPage.getUsuarioLogado(), Messages.MSG_USUARIO_LOGADO);

        loginPage.sairSistema();
    }
}
