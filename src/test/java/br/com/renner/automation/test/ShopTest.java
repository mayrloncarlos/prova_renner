package br.com.renner.automation.test;

import br.com.renner.automation.common.Messages;
import br.com.renner.automation.core.BaseTestCase;
import br.com.renner.automation.model.LoginModel;
import br.com.renner.automation.model.ShopModel;
import br.com.renner.automation.pages.LoginPage;
import br.com.renner.automation.pages.ShopPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ShopTest extends BaseTestCase {

    @Test
    public void deveRealizarUmaCompra(){

        ShopModel shopModel = ShopModel.gerarInformacoesShopModel();
        LoginModel loginModel = LoginModel.gerarInformacoesLoginModel();
        ShopPage shopPage = new ShopPage(shopModel);
        LoginPage loginPage = new LoginPage(loginModel);

        loginPage.criarNovoUsuario();

        shopPage.pesquisarProduto1();
        shopPage.adicionarProduto1NoCarrinho();

        Assert.assertEquals(shopPage.obterMsgAddToCartSuccess(), Messages.MSG_ADD_TO_CART_SUCCESS);

        shopPage.pesquisarProduto2();
        shopPage.adicionarProduto2NoCarrinho();

        Assert.assertEquals(shopPage.obterMsgAddToCartSuccess(), Messages.MSG_ADD_TO_CART_SUCCESS);

        shopPage.pesquisarProduto3();
        shopPage.adicionarProduto3NoCarrinho();

        Assert.assertEquals(shopPage.obterMsgAddToCartSuccess(), Messages.MSG_ADD_TO_CART_SUCCESS);

        shopPage.finalizarCompra();

        Assert.assertEquals(shopPage.obterMsgCompraEfetuadaComSucesso(), Messages.MSG_COMPRA_EFETUDA);

        loginPage.sairSistema();
    }
}
