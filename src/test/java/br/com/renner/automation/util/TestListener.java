package br.com.renner.automation.util;
import org.testng.*;
import br.com.renner.automation.common.*;
import br.com.renner.automation.enums.BrowserType;

public class TestListener implements ITestListener, ISuiteListener, IInvokedMethodListener, IRetryAnalyzer{


	private static ThreadLocal<Long> START_TIME = new ThreadLocal<>();

	public int contadorRetry = 0;

	@Override
	public void onTestStart(ITestResult result) {
		Browser.startDriver(Parametro.BROWSER.get(), Property.TIPO_EXECUCAO);

		//Contador
		START_TIME.set(System.currentTimeMillis());
	}

	@Override
	public void onTestSuccess(ITestResult result) {
	}

	@Override
	public void onTestFailure(ITestResult result) {
	}

	@Override
	public void onTestSkipped(ITestResult result) {

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	@Override
	public void onStart(ITestContext context) {
		for (ITestNGMethod method : context.getAllTestMethods()) {
	          method.setRetryAnalyzer(new TestListener());
	      }
		
		if (context.getCurrentXmlTest().getParameter("browser") != null) {
			Parametro.BROWSER.set(BrowserType.valueOf(context.getCurrentXmlTest().getParameter("browser")));
		}
	}

	@Override
	public void onStart(ISuite suite) {
		//Configuration Reader
		ConfigReader.loadProperties();
	}

	@Override
	public void onFinish(ITestContext context) {
	}

	@Override
	public void onFinish(ISuite suite) {

	}
	
	@Override
	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
		if(method.isTestMethod()) {
			Browser.goToUrl(Property.SITE_ADDRESS);
			Browser.maximize();
			
			if(Property.DIMENSAO_PERSONALIZADA) {
				Browser.dimensaoPersonalizada();
			}
			
			//Contador
			START_TIME.set(System.currentTimeMillis());
		}
	}

	@Override
	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
		if(method.isTestMethod()) {
			Selenium.resetDriver();
		}
	}

	@Override
	public boolean retry(ITestResult result) {
		if(contadorRetry < Property.RETRY_LIMIT) {
			result.getTestContext().getSkippedTests().removeResult(result);
			contadorRetry++;
			return true;
		}
		return false;
	}

}