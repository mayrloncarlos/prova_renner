package br.com.renner.automation.util;

import java.util.Random;

public class GenerateRandom {

    public static Integer gerarNumeroRandomico(){
        // create instance of Random class
        Random rand = new Random();

        // Generate random integers in range 0 to 999
        return rand.nextInt(9999999);
    }

}
