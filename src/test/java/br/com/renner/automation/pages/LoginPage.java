package br.com.renner.automation.pages;

import br.com.renner.automation.core.BasePage;
import br.com.renner.automation.model.LoginModel;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    private LoginModel informacoesLogin;

    public LoginPage(LoginModel informacoesLogin){
        this.informacoesLogin = informacoesLogin;
    }

    public LoginPage(){
    }

    @FindBy(xpath = "//a[@class='login']")
    private WebElement btnSignIn;

    @FindBy(id = "email_create")
    private WebElement inputEmailCreate;

    @FindBy(id = "SubmitCreate")
    private WebElement btnSubmitCreateAccount;

    @FindBy(id = "id_gender1")
    private WebElement radioGenderMr;

    @FindBy(id = "customer_firstname")
    private WebElement inputFirstName;

    @FindBy(id = "customer_lastname")
    private WebElement inputLastName;

    @FindBy(id = "passwd")
    private WebElement inputPassword;

    @FindBy(id = "firstname")
    private WebElement inputAddressFirstName;

    @FindBy(id = "lastname")
    private WebElement inputAddressLastName;

    @FindBy(id = "address1")
    private WebElement inputAddress1;

    @FindBy(id = "city")
    private WebElement inputCity;

    @FindBy(id = "id_state")
    private WebElement selectState;

    @FindBy(id = "postcode")
    private WebElement inputZipCode;

    @FindBy(id = "id_country")
    private WebElement inputCountry;

    @FindBy(id = "phone_mobile")
    private WebElement inputPhone;

    @FindBy(id = "submitAccount")
    private WebElement btnSubmit;

    @FindBy(xpath = "//a[@title='View my customer account']")
    private WebElement usuarioLogado;

    @FindBy(xpath = "//a[@title='Log me out']")
    private WebElement deslogarSistema;

    //MÉTODOS GERAIS
    public void criarNovoUsuario(){
        clicarBotaoSignIn();
        preencherCampoEmailPreCadastro();
        clicarBotaoSubmitCreateAccount();

        clicarRadioMr();
        preencherFirstName();
        preencherLastName();
        preencherPassword();
        preencherFirstNameAddress();
        preencherLastNameAddress();
        preencherAddress();
        preencherCity();
        preencherState();
        preencherZipCode();
        preencherCountry();
        preencherPhone();
        clicarBotaoRegistrar();
    }

    //CLICKS
    public void sairSistema(){
        click(deslogarSistema);
    }

    private void clicarBotaoRegistrar(){
        click(btnSubmit);
    }
    private void clicarBotaoSignIn(){
        click(btnSignIn);
    }

    private void clicarBotaoSubmitCreateAccount(){
        click(btnSubmitCreateAccount);
    }

    private void clicarRadioMr(){
        click(radioGenderMr);
    }

    //PREENCHIMENTO DE CAMPOS
    private void preencherCampoEmailPreCadastro(){
        preencherCampo(inputEmailCreate, informacoesLogin.getEmailPreCadastro());
    }

    private void preencherFirstName(){
        preencherCampo(inputFirstName, informacoesLogin.getFirstName());
    }

    private void preencherLastName(){
        preencherCampo(inputLastName, informacoesLogin.getLastName());
    }

    private void preencherPassword(){
        preencherCampo(inputPassword, informacoesLogin.getPassword());
    }

    private void preencherFirstNameAddress(){
        preencherCampo(inputAddressFirstName, informacoesLogin.getFirsNameAddress());
    }

    private void preencherLastNameAddress(){
        preencherCampo(inputAddressLastName, informacoesLogin.getLastNameAddress());
    }

    private void preencherAddress(){
        preencherCampo(inputAddress1, informacoesLogin.getAddress());
    }

    private void preencherCity(){
        preencherCampo(inputCity, informacoesLogin.getCity());
    }

    private void preencherState(){
        selecionarItemDropdown(selectState, informacoesLogin.getState());
    }

    private void preencherZipCode(){
        preencherCampo(inputZipCode, informacoesLogin.getZipCode());
    }

    private void preencherCountry(){
        selecionarItemDropdown(inputCountry, informacoesLogin.getCountry());
    }

    private void preencherPhone(){
        preencherCampo(inputPhone, informacoesLogin.getPhone());
    }

    //OBTER TEXTO - GET
    public String getUsuarioLogado(){
        return obterTexto(usuarioLogado);
    }

}
