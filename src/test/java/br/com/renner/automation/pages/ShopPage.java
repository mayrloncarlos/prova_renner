package br.com.renner.automation.pages;

import br.com.renner.automation.core.BasePage;
import br.com.renner.automation.model.ShopModel;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShopPage extends BasePage {

    private ShopModel informacoesShop;

    public ShopPage(ShopModel informacoesShop){
        this.informacoesShop = informacoesShop;
    }

    public ShopPage(){
    }

    @FindBy(id = "search_query_top")
    private WebElement inputSearch;

    @FindBy(xpath = "//button[@name='submit_search']")
    private WebElement submitSearch;

    @FindBy(xpath = "//h5[@itemprop='name']//a[@title='Printed Chiffon Dress']")
    private WebElement produto1;

    @FindBy(xpath = "//h5[@itemprop='name']//a[@title='Faded Short Sleeve T-shirts']")
    private WebElement produto2;

    @FindBy(xpath = "//h5[@itemprop='name']//a[@title='Blouse']")
    private WebElement produto3;

    @FindBy(xpath = "//a[@title='Green']")
    private WebElement corVerde;

    @FindBy(xpath = "//a[@title='Blue']")
    private WebElement corAzul;

    @FindBy(id = "group_1")
    private WebElement selectTamanho;

    @FindBy(xpath = "//button[@name='Submit']")
    private WebElement addToCart;

    @FindBy(xpath = "//span[@title='Continue shopping']")
    private WebElement btnContinueShopping;

    @FindBy(xpath = "//div[@class='layer_cart_product col-xs-12 col-md-6']//h2")
    private WebElement productSuccessAdd;

    @FindBy(xpath = "//a[@class='btn btn-default button-plus product_quantity_up']")
    private WebElement btnIncrementarQuantidade;

    @FindBy(xpath = "//a[@title='Proceed to checkout']")
    private WebElement btnProceedToCheckout;

    @FindBy(xpath = "//p[@class='cart_navigation clearfix']/a[@title='Proceed to checkout']")
    private WebElement btnProceedSummary;

    @FindBy(xpath = "//button[@name='processAddress']")
    private WebElement btnProceedAddress;

    @FindBy(id = "uniform-cgv")
    private WebElement chkboxTermos;

    @FindBy(xpath = "//button[@name='processCarrier']")
    private WebElement btnProceedCarrier;

    @FindBy(xpath = "//a[@title='Pay by bank wire']")
    private WebElement btnPagarComCartao;

    @FindBy(xpath = "//button[@class='button btn btn-default button-medium']")
    private WebElement btnConfirmarCompra;

    @FindBy(xpath = "//div[@class='box']/p/strong")
    private WebElement msgCompraEfetuada;

    //MÉTODOS GERAIS
    public void adicionarProduto1NoCarrinho(){
        clicarProduto1();
        selecionarTamanhoM();
        selecionarCorVerde();
        clicarBotaoAddToCart();
        clicarBotaoContinueShopping();
    }

    public void adicionarProduto2NoCarrinho(){
        clicarProduto2();
        selecionarCorAzul();
        clicarBotaoAddToCart();
        clicarBotaoContinueShopping();

    }
    public void adicionarProduto3NoCarrinho(){
        clicarProduto3();
        clicarBotaoIncrementarQuantidade();
        clicarBotaoAddToCart();
    }

    public void finalizarCompra(){
        clicarBotaoProceedToCheckout();
        clicarBotaoProceedSummary();
        clicarBotaoProceedAddress();
        aceitarTermos();
        clicarBotaoProceedCarrier();
        clicarBotaoPagarComCartao();
        clicarBotaoConfirmarCompra();
    }


    private void selecionarTamanhoM(){
        selecionarItemDropdown(selectTamanho, "2");
    }

    private void selecionarCorVerde(){
        click(corVerde);
    }

    private void selecionarCorAzul(){
        click(corAzul);
    }

    //CLICKS
    private void clicarBotaoConfirmarCompra(){
        click(btnConfirmarCompra);
    }

    private void clicarBotaoPagarComCartao(){
        click(btnPagarComCartao);
    }

    private void aceitarTermos(){
        click(chkboxTermos);
    }

    private void clicarBotaoProceedCarrier(){
        click(btnProceedCarrier);
    }

    private void clicarBotaoProceedAddress(){
        click(btnProceedAddress);
    }

    private void clicarBotaoProceedSummary(){
        click(btnProceedSummary);
    }

    private void clicarBotaoProceedToCheckout(){
        click(btnProceedToCheckout);
    }

    private void clicarBotaoIncrementarQuantidade(){
        click(btnIncrementarQuantidade);
    }

    private void clicarBotaoContinueShopping(){
        click(btnContinueShopping);
    }

    private void clicarBotaoAddToCart(){
        click(addToCart);
    }

    private void clicarBotaoPesquisar(){
        click(submitSearch);
    }

    private void clicarProduto1(){
        click(produto1);
    }

    private void clicarProduto2(){
        click(produto2);
    }

    private void clicarProduto3(){
        click(produto3);
    }

    // PREENCHIMENTO DE CAMPO
    public void pesquisarProduto1(){
        preencherCampo(inputSearch, informacoesShop.getPrintedChiffonDress());
        clicarBotaoPesquisar();
    }

    public void pesquisarProduto2(){
        limparCampo(inputSearch);
        preencherCampo(inputSearch, informacoesShop.getFadedShortSleeve());
        clicarBotaoPesquisar();
    }

    public void pesquisarProduto3(){
        limparCampo(inputSearch);
        preencherCampo(inputSearch, informacoesShop.getBlouse());
        clicarBotaoPesquisar();
    }

    //OBTER TEXTO - GET
    public String obterMsgAddToCartSuccess(){
        return obterTexto(productSuccessAdd);
    }

    public String obterMsgCompraEfetuadaComSucesso(){
        return obterTexto(msgCompraEfetuada);
    }
}
